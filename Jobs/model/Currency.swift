//
//  Currency.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

enum Currency: String, Codable {
    case eur = "EUR"
    case usd
    
    var symbol: String {
        switch self {
        case .eur:
            return "€"
        case .usd:
            return "$"
        }
    }
}

