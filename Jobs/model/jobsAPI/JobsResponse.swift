//
//  JobsResponse.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct JobsResponse: Decodable {
    let data: [JobsByDate]

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let innerDictionary = try container.decode([String : [Job]].self, forKey: .data)
        data = try innerDictionary.map { dateString, data in
            let formatter = DateFormatter.short
            guard let date = formatter.date(from: dateString) else {
                throw DecodingError.dataCorruptedError(forKey: .data, in: container, debugDescription: "Failed to parse date \(dateString) into a string")
            }
            let jobsByDate = JobsByDate(date: date, jobs: data)
            return jobsByDate
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case data
    }
}
