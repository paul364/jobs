//
//  Job.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct Job: Decodable {
    let id: Int
    let title: String
    let location: Location
    let client: Client
    let jobCategory: JobCategory
    let photoURL: String?
    let shifts: [Shift]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case location
        case client
        case jobCategory = "job_category"
        case photoURL = "photo"
        case shifts
    }
}
