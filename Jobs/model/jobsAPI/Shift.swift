//
//  Shift.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct Shift: Codable {
    let id: String
    let hourlyRate: Decimal
    let duration: Int
    let currency: Currency
    let startDate: Date
    let endDate: Date
    
    private enum CodingKeys: String, CodingKey {
        case id
        case hourlyRate = "earnings_per_hour"
        case duration
        case currency
        case startDate = "start_datetime"
        case endDate = "end_datetime"
    }
}
