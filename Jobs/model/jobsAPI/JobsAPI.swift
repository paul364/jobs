//
//  JobsAPI.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import RxSwift
import RxAlamofire
import Alamofire

protocol JobsAPI {
    func jobs(for date: Date) -> Observable<[JobsByDate]>
}

struct JobsAPIImpl: API {
    private func jobsURL(version: APIVersion) -> String {
        return "\(baseURL(version: version))/contractor/shifts" //https://temper.works/api/v2/contractor/shifts?dates=2019-10-04
    }
}

extension JobsAPIImpl: JobsAPI {
    func jobs(for date: Date) -> Observable<[JobsByDate]> {
        let dateString = DateFormatter.short.string(from: date)
        let observable = Observable<[JobsByDate]>.create { observer in
            RxAlamofire.requestData(.get, self.jobsURL(version: .v2), parameters: ["dates" : dateString])
            .subscribe(onNext: { (response, data) in
                NSLog("Jobs request response status code: \(response.statusCode)")
                let parser = APIResponseParserImpl()
                do {
                    let response = try parser.parse(data: data, as: JobsResponse.self)
                    observer.onNext(response.data)
                    observer.onCompleted()
                } catch {
                    observer.onError(error)
                }
            }, onError: { error in
                observer.onError(error)
            }, onCompleted: {
                observer.onCompleted()
            })
            return Disposables.create()
        }
        return observable
    }
}
