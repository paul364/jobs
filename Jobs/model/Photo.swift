//
//  Photo.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct Photo: Codable {
    let formats: [Format]

    struct Format: Codable {
        let cdnURL: String
        
        private enum CodingKeys: String, CodingKey {
            case cdnURL = "cdn_url"
        }
    }
}
