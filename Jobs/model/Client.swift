//
//  Client.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct Client: Codable {
    let id: String
    let name: String
    let photos: [Photo]
}
