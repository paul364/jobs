//
//  API.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

enum APIVersion: String {
    case v1
    case v2
}

protocol API {}

extension API {
    func baseURL(version: APIVersion) -> String {
        return "https://temper.works/api/\(version.rawValue)"
    }
}
