//
//  APIResponseParser.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

protocol APIResponseParser {
    func parse<T: Decodable>(data: Data, as type: T.Type, decoder: JSONDecoder) throws -> T
}

struct APIResponseParserImpl {}

extension APIResponseParserImpl: APIResponseParser {
    func parse<T: Decodable>(data: Data, as type: T.Type, decoder: JSONDecoder = .smartDateDecoder) throws -> T {
        let object = try decoder.decode(type, from: data)
        return object
    }
}
