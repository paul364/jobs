//
//  Decimal+.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

extension Decimal {
    func stringValue(numberStyle: NumberFormatter.Style = .decimal, fractionDigits: Int, withGroupingSeparator: Bool = false, positivePrefix: String? = nil, negativePrefix: String? = nil) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = numberStyle
        formatter.minimumFractionDigits = fractionDigits
        formatter.maximumFractionDigits = fractionDigits
        formatter.usesGroupingSeparator = withGroupingSeparator
        formatter.locale = Locale(identifier: "NL_nl")
        formatter.negativePrefix = negativePrefix
        formatter.positivePrefix = positivePrefix
        let number = NSDecimalNumber(decimal: self)
        return formatter.string(from: number) ?? ""
    }
}
