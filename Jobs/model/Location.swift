//
//  Location.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import Foundation

struct Location: Codable {
    let latitude: Double?
    let longitude: Double?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let latitude: Double?
        if let latitudeString = try container.decodeIfPresent(String.self, forKey: .latitude) {
            guard let lat = Double(latitudeString) else {
                throw DecodingError.dataCorruptedError(forKey: .latitude, in: container, debugDescription: "Failed to parse latitude")
            }
            latitude = lat
        } else {
            latitude = nil
        }
        
        let longitude: Double?
        if let longitudeString = try container.decodeIfPresent(String.self, forKey: .longitude) {
            guard let lon = Double(longitudeString) else {
                throw DecodingError.dataCorruptedError(forKey: .latitude, in: container, debugDescription: "Failed to parse longitude")
            }
            longitude = lon
        } else {
            longitude = nil
        }
        
        self.latitude = latitude
        self.longitude = longitude
    }
    
    private enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
