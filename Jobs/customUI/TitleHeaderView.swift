//
//  TitleHeaderView.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

final class TitleHeaderView: UICollectionReusableView {
    @IBOutlet private var titleLabel: UILabel! {
        didSet {
            titleLabel.font = .themeLight18
        }
    }
}

extension TitleHeaderView {
    static let identifier = "TitleHeader"
    static var nibName = "TitleHeaderView"
    
    func update(_ title: String) {
        titleLabel.text = title
    }
}
