//
//  UIFont+.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

extension UIFont {
    static let themeRegular14: UIFont = .themeRegular(14)
    static let themeRegular16: UIFont = .themeRegular(16)
    
    static let themeLight18: UIFont = .themeLight(18)
    
    static let themeBold14: UIFont = .themeBold(14)
    static let themeBold16: UIFont = .themeBold(16)
    static let themeBold18: UIFont = .themeBold(18)
    static let themeBold22: UIFont = .themeBold(22)
    
    static func themeRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: .regular, size: size)
    }
    
    static func themeLight(_ size: CGFloat) -> UIFont {
        return UIFont(name: .light, size: size)
    }
    
    static func themeBold(_ size: CGFloat) -> UIFont {
        return UIFont(name: .bold, size: size)
    }
    
    // MARK: Private
    private convenience init(name: ThemeName, size: CGFloat) {
        self.init(name: name.rawValue, size: size)!
    }
    
    private enum ThemeName: String {
        case regular = "Lato-Regular"
        case bold = "Lato-Bold"
        case light = "Lato-Light"
    }
}
