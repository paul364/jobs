//
//  CustomizableButton.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

class CustomizableButton: UIButton, Maskable {
    private let highlightedBackgroundLayer = CALayer()
    
    var style: Style = .green {
        didSet {
            applyCustomLayout(style: style)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        applyCustomLayout(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        applyCustomLayout(style: style)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyCustomLayout(style: style)
    }
    
    override var buttonType: UIButton.ButtonType {
        return .custom
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            highlightedBackgroundLayer.isHidden = !newValue
            CATransaction.commit()
            super.isHighlighted = newValue
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch style {
        case .masked(style: _, roundedCorners: let corners):
            mask(roundedCorners: corners, cornerRadius: min(bounds.size.width, bounds.size.height) / 2)
        default:
            layer.cornerRadius = min(bounds.size.width, bounds.size.height) / 10
        }
        highlightedBackgroundLayer.frame = layer.bounds
    }
    
    private func applyCustomLayout(style: Style) {
        isExclusiveTouch = true
        layer.masksToBounds = true
        setupHighlightedBackgroundLayer(highlightedBackgroundLayer)
        switch style {
        case .green:
            applyCustomLayout(backgroundColor: .themeGreen, titleColor: .black)
        case .white:
            applyCustomLayout(backgroundColor: .white, titleColor: .black, borderColor: .black, borderWidth: 1)
        case .custom(backgroundColor: let bgColor, textColor: let txtColor):
            applyCustomLayout(backgroundColor: bgColor, titleColor: txtColor)
        case .masked(style: let style, roundedCorners: _):
            applyCustomLayout(style: style)
        }
    }
    
    private func applyCustomLayout(backgroundColor: UIColor, titleColor: UIColor, borderColor: UIColor? = nil, borderWidth: CGFloat = 0) {
        self.backgroundColor = backgroundColor
        setBackgroundImage(backgroundColor.image, for: .normal)
        highlightedBackgroundLayer.backgroundColor = backgroundColor.inversed.cgColor
        setTitleColor(titleColor, for: .normal)
        setTitleColor(titleColor.withAlphaComponent(0.5), for: .disabled)
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
    }
    
    private func setupHighlightedBackgroundLayer(_ layer: CALayer) {
        layer.frame = layer.bounds
        layer.isHidden = !isHighlighted
        self.layer.insertSublayer(layer, at: 0)
    }
}

// MARK: STYLE
extension CustomizableButton {
    indirect enum Style {
        case green
        case white
        case custom(backgroundColor: UIColor, textColor: UIColor)
        case masked(style: Style, roundedCorners: UIRectCorner)
    }
}
