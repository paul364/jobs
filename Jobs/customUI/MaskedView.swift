//
//  MaskedView.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

class MaskedView: UIView {
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let curveStartPoint = CGPoint(x: 0, y: rect.height)
        let curveEndPoint = CGPoint(x: rect.width / 4, y: 0)
        let curveControlPoint1 = CGPoint(x: rect.width / 6, y: rect.height)
        let curveControlPoint2 = CGPoint(x: rect.width / 10, y: 0)
        path.move(to: curveStartPoint)
        path.addCurve(to: curveEndPoint, controlPoint1: curveControlPoint1, controlPoint2: curveControlPoint2)
        path.addLine(to: CGPoint(x: rect.width, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.close()
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
}
