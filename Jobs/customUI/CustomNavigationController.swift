//
//  CustomNavigationController.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar(navigationBar)
    }
    
    private func setupNavigationBar(_ navigationBar: UINavigationBar) {
        navigationBar.barStyle = .black
        navigationBar.tintColor = .themeGreen
        navigationBar.titleTextAttributes = [
            .foregroundColor : UIColor.white,
            .font : UIFont.themeBold22
        ]
    }
}
