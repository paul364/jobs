//
//  UIColor+.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

// MARK: THEME COLORS
extension UIColor {
    static let themeGreen = UIColor(hex: 0x2AFF86)
    static let themeViolet = UIColor(hex: 0x6411DE)
    static let themeGray = UIColor(hex: 0xF7F7F7)
}

// MARK: HEX
extension UIColor {
    convenience init(hex: UInt) {
        self.init(hex: hex, alpha: 1)
    }
    
    convenience init(hex: UInt, alpha: CGFloat) {
        let red: CGFloat = CGFloat((hex >> 16) & 0xff)
        let green: CGFloat = CGFloat((hex >> 8) & 0xff)
        let blue: CGFloat = CGFloat(hex & 0xff)
        
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
}

// MARK: INVERSION
extension UIColor {
    var inversed: UIColor {
        var alpha: CGFloat = 1.0

        var white: CGFloat = 0.0
        if getWhite(&white, alpha: &alpha) {
            return UIColor(white: 1.0 - white, alpha: alpha)
        }

        var hue: CGFloat = 0.0, saturation: CGFloat = 0.0, brightness: CGFloat = 0.0
        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor(hue: 1.0 - hue, saturation: 1.0 - saturation, brightness: 1.0 - brightness, alpha: alpha)
        }

        var red: CGFloat = 0.0, green: CGFloat = 0.0, blue: CGFloat = 0.0
        if getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: 1.0 - red, green: 1.0 - green, blue: 1.0 - blue, alpha: alpha)
        }

        return self
    }
}

//MARK: IMAGE
extension UIColor {
    var image: UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}
