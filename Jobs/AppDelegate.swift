//
//  AppDelegate.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder {
    var window: UIWindow?

    private func setInitialScreen() {
        let isRunningUnitTests = NSClassFromString("XCTest") != nil
        guard !isRunningUnitTests else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        let jobsVC = JobsViewController.instantiate()
        window?.rootViewController = jobsVC
        window?.makeKeyAndVisible()
    }
}


extension AppDelegate: UIApplicationDelegate  {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setInitialScreen()
        return true
    }
}
