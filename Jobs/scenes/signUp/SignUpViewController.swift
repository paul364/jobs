//
//  SignUpViewController.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit

final class SignUpViewController: UIViewController {
    private static let storyboardName = "SignUp"
    
    @IBAction private func onDoneAction(_ sender: UIBarButtonItem) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController {
    static func instantiate() -> UIViewController {
        let navigationController = UIStoryboard(name: storyboardName, bundle: nil).instantiateInitialViewController() as! UINavigationController
        return navigationController
    }
}
