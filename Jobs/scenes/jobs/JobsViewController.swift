//
//  JobsViewController.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit
import RxSwift
import MBProgressHUD

final class JobsViewController: UIViewController {
    private static let storyboardName = "JobsOverview"
    
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var signUpButton: CustomizableButton!
    @IBOutlet private var loginButton: CustomizableButton!
    @IBOutlet private var filtersButton: CustomizableButton!
    @IBOutlet private var mapButton: CustomizableButton!
    
    private let bag = DisposeBag()
    private var viewModel: JobsViewModelOutput!
    
    private var contentOffsetHeightRatio: CGFloat?
    
    private var hud: MBProgressHUD {
        let hud = MBProgressHUD(for: view) ?? MBProgressHUD.showAdded(to: view, animated: true)
        hud.contentColor = .black
        return hud
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        bindUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView.collectionViewLayout.invalidateLayout()
        contentOffsetHeightRatio = collectionView.contentOffset.y / collectionView.bounds.height
    }
    
    // MARK: ACTIONS
    @IBAction private func onSignUpAction(_ sender: UIButton) {
        let signUpVC = SignUpViewController.instantiate()
        present(signUpVC, animated: true, completion: nil)
    }
    
    @IBAction private func onLoginAction(_ sender: UIButton) {
        let loginVC = LoginViewController.instantiate()
        present(loginVC, animated: true, completion: nil)
    }
    
    @objc private func onRefresh(_ sender: UIRefreshControl) {
        viewModel.loadJobs(for: Date())
    }
    
    private func bindUI() {
        viewModel = JobsViewModel()
        viewModel.loadJobs(for: Date())
        viewModel.jobs.asDriver()
            .drive(onNext: { [weak self] newData in
                self?.collectionView.refreshControl?.endRefreshing()
                self?.collectionView.reloadData()
            })
            .disposed(by: bag)
        
        viewModel.onShowLoadingHud
            .drive(onNext: { [weak self] newValue in
                let hud = self?.hud
                if newValue {
                    hud?.mode = .indeterminate
                    hud?.label.text = "Loading"
                } else {
                    hud?.hide(animated: true)
                }
            })
            .disposed(by: bag)
        
        viewModel.onShowError
            .drive(onNext: { [weak self] errorDescription in
                let hud = self?.hud
                hud?.mode = .text
                hud?.label.text = errorDescription
                hud?.hide(animated: true, afterDelay: 1)
            })
            .disposed(by: bag)
    }

    // MARK: UI SETUP
    private func setup() {
        setupSignUpButton(signUpButton)
        setupLoginButton(loginButton)
        setupFiltersButton(filtersButton)
        setupMapButton(mapButton)
        setupCollectionView(collectionView)
    }
    
    private func setupSignUpButton(_ button: CustomizableButton) {
        button.style = .green
        button.setTitle("Sign up", for: .normal)
        button.titleLabel?.font = .themeRegular16
    }
    
    private func setupLoginButton(_ button: CustomizableButton) {
        button.style = .white
        button.setTitle("Log in", for: .normal)
        button.titleLabel?.font = .themeRegular16
    }
    
    private func setupFiltersButton(_ button: CustomizableButton) {
        button.style = .masked(style: .custom(backgroundColor: .themeGray, textColor: .black), roundedCorners: [.bottomLeft, .topLeft])
        button.setTitle("Filters", for: .normal)
        button.titleLabel?.font = .themeRegular14
        button.titleLabel?.textAlignment = .right
    }
    
    private func setupMapButton(_ button: CustomizableButton) {
        button.style = .masked(style: .custom(backgroundColor: .themeGray, textColor: .black), roundedCorners: [.bottomRight, .topRight])
        button.setTitle("Kaart", for: .normal)
        button.titleLabel?.font = .themeRegular14
        button.titleLabel?.textAlignment = .right
    }
    
    private func setupCollectionView(_ collectionView: UICollectionView) {
        let nib = UINib(nibName: TitleHeaderView.nibName, bundle: nil)
        collectionView.register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TitleHeaderView.identifier)
        
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .themeGreen
        refreshControl.addTarget(self, action: #selector(onRefresh(_:)), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    private func headerSize(for collectionView: UICollectionView) -> CGSize {
        let size: CGSize
        if collectionView.bounds.width > collectionView.bounds.height {
            // Landscape
            size = CGSize(width: collectionView.bounds.width / 10, height: collectionView.bounds.height)
        } else {
            size = CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height / 10)
        }
        return size
    }
}

// MARK: COLLECTION VIEW DELEGATE
extension JobsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        guard let ratio = contentOffsetHeightRatio else {
            return proposedContentOffset
        }
        contentOffsetHeightRatio = nil
        
        let newOffset = CGPoint(x: 0, y: ratio * collectionView.bounds.height)
        return newOffset
    }
}

// MARK: COLLECTION VIEW DATASOURCE
extension JobsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.jobs.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.jobs.value[section].jobs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let jobCell = collectionView.dequeueReusableCell(withReuseIdentifier: JobCollectionViewCell.identifier, for: indexPath) as! JobCollectionViewCell
        let job = viewModel.jobs.value[indexPath.section].jobs[indexPath.row]
        jobCell.update(job)
        return jobCell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: TitleHeaderView.identifier, for: indexPath) as! TitleHeaderView
        let date = viewModel.jobs.value[indexPath.section].date
        headerView.update(DateFormatter.day.string(from: date))
        headerView.frame.size = headerSize(for: collectionView)
        return headerView
    }
}

// MARK: COLLECTION VIEW FLOW
extension JobsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize
        if collectionView.bounds.width > collectionView.bounds.height {
            // Landscape
            size = CGSize(width: collectionView.bounds.width / 2.5, height: collectionView.bounds.height)
        } else {
            size = CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height / 2.5)
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return headerSize(for: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension JobsViewController {
    static func instantiate() -> JobsViewController {
        let jobsVC = UIStoryboard(name: storyboardName, bundle: nil).instantiateInitialViewController() as! JobsViewController
        return jobsVC
    }
}

