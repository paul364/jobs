//
//  JobsViewModel.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import RxSwift
import RxCocoa

protocol JobsViewModelOutput {
    var jobs: BehaviorRelay<[JobsByDate]> { get }
    var onShowLoadingHud: Driver<Bool> { get }
    var onShowError: Driver<String> { get }
    func loadJobs(for date: Date)
}

struct JobsViewModel {
    // MARK: - Input
    private let api: JobsAPI

    // MARK: - Output
    let jobs = BehaviorRelay<[JobsByDate]>(value: [])
    
    private let isLoading = BehaviorRelay<Bool>(value: false)
    private let shouldShowError = BehaviorRelay<String>(value: "")
    private let bag = DisposeBag()

    // MARK: - Init
    init(api: JobsAPI = JobsAPIImpl()) {
        self.api = api
    }
}

extension JobsViewModel: JobsViewModelOutput {
    var onShowLoadingHud: Driver<Bool> {
        return isLoading.asDriver().distinctUntilChanged()
    }
    
    var onShowError: Driver<String> {
        return shouldShowError.asDriver()
    }
    
    func loadJobs(for date: Date) {
        isLoading.accept(true)
        
        let apiJobs = api.jobs(for: date).share()
        apiJobs.catchErrorJustReturn([])
            .debug()
            .bind(to: jobs)
            .disposed(by: bag)
        
        apiJobs
            .debug()
            .subscribe(onNext: { jobs in
                self.isLoading.accept(false)
            }, onError: { (error) in
                self.isLoading.accept(false)
                self.shouldShowError.accept("Failed to load :(")
            })
            .disposed(by: bag)
    }
}
