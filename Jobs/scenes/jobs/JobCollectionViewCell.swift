//
//  JobCollectionViewCell.swift
//  Jobs
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import UIKit
import Kingfisher

final class JobCollectionViewCell: UICollectionViewCell {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var rateLabel: UILabel! {
        didSet {
            rateLabel.font = .themeBold16
        }
    }
    @IBOutlet private var responsibilitiesLabel: UILabel! {
        didSet {
            responsibilitiesLabel.textColor = .themeViolet
            responsibilitiesLabel.font = .themeBold14
        }
    }
    @IBOutlet private var employerLabel: UILabel! {
        didSet {
            employerLabel.font = .themeBold18
        }
    }
    @IBOutlet private var workingHoursLabel: UILabel! {
        didSet {
            workingHoursLabel.font = .themeRegular16
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = min(imageView.bounds.size.width, imageView.bounds.size.height) / 20
    }
}

extension JobCollectionViewCell {
    static let identifier = "Job"
    
    func update(_ job: Job) {
        var url: URL?
        if let urlString = job.photoURL ?? job.client.photos.first?.formats.first?.cdnURL {
            url = URL(string: urlString)
        }
        imageView.kf.setImage(with: url, placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.4))])
        
        let rate: String
        let workingHours: String?
        if let shift = job.shifts.first {
            rate = "\(shift.currency.symbol) \(shift.hourlyRate.stringValue(fractionDigits: 2))"
            let formatter = DateFormatter.time
            workingHours = String(format: "%@ - %@", formatter.string(from: shift.startDate), formatter.string(from: shift.endDate))
        } else {
            rate = "Unknown"
            workingHours = nil
        }
        rateLabel.text = rate
        workingHoursLabel.text = workingHours
        
        responsibilitiesLabel.text = job.jobCategory.description.uppercased()
        employerLabel.text = job.client.name
    }
}
