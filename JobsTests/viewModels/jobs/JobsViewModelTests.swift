//
//  JobsViewModelTests.swift
//  JobsTests
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import XCTest
import RxSwift
import RxBlocking
import RxTest
@testable import Jobs

final class JobsViewModelTests: XCTestCase {
    var jobsAPIMock: JobsAPIMock!
    var viewModel: JobsViewModel!
    
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        jobsAPIMock = JobsAPIMock()
        viewModel = JobsViewModel(api: jobsAPIMock)
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }
    
    func testLoadingObservableStartsAtFalse() {
        XCTAssertEqual(try viewModel.onShowLoadingHud.toBlocking().first(), false)
    }
    
    func testErrorObservableStartsEmpty() {
        XCTAssert(try viewModel.onShowError.toBlocking().first()!.isEmpty)
    }
    
    func testLoadingObservableFalseAfterJobsLoading() {
        viewModel.loadJobs(for: Date())
        let isLoading = scheduler.createObserver(Bool.self)

        viewModel.onShowLoadingHud
            .drive(isLoading)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(isLoading.events, [
          .next(0, false)
        ])
    }
    
    func testErrorObservableOnSuccessfulJobsLoading() {
        jobsAPIMock.requestIsSuccessful = true
        viewModel.loadJobs(for: Date())
        
        let isShowingError = scheduler.createObserver(Bool.self)
        
        viewModel.onShowError
            .map { !$0.isEmpty }
            .drive(isShowingError)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(isShowingError.events, [
          .next(0, false)
        ])
    }
    
    func testErrorObservableOnUnsuccessfulJobsLoading() {
        jobsAPIMock.requestIsSuccessful = false
        viewModel.loadJobs(for: Date())
        
        let isShowingError = scheduler.createObserver(Bool.self)
        
        viewModel.onShowError
            .map { !$0.isEmpty }
            .drive(isShowingError)
            .disposed(by: disposeBag)
        
        XCTAssertEqual(isShowingError.events, [
          .next(0, true)
        ])
    }
}

// MARK : MOCK
class JobsAPIMock: JobsAPI {
    var requestIsSuccessful = true
    func jobs(for date: Date) -> Observable<[JobsByDate]> {
        let observable = Observable<[JobsByDate]>.create { [weak self] observer in
            if let isSuccessful = self?.requestIsSuccessful, isSuccessful {
                observer.onNext([])
                observer.onCompleted()
            } else {
                observer.onError(RxError.unknown)
            }
            return Disposables.create()
        }
        return observable
    }
}
