//
//  APIResponseParserTests.swift
//  JobsTests
//
//  Created by Somebody Someone on 06/10/2019.
//  Copyright © 2019 Assignment. All rights reserved.
//

import XCTest
@testable import Jobs

final class APIResponseParserTests: XCTestCase {
    let parser: APIResponseParser = APIResponseParserImpl()
    
    func testClientParsing() {
        let clientJSONData = jsonData(fromFileNamed: "Client")
        do {
            let client = try parser.parse(data: clientJSONData, as: Client.self, decoder: .smartDateDecoder)
            XCTAssert(client.id == "87aqwq")
            XCTAssert(client.name == "Centraal Ketelhuis // Teka")
            XCTAssert(client.photos.count == 2)
        } catch let decodingError as DecodingError {
            XCTFail(decodingError.description)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testShiftParsing() {
        let shiftJSONData = jsonData(fromFileNamed: "Shift")
        do {
            let shift = try parser.parse(data: shiftJSONData, as: Shift.self, decoder: .smartDateDecoder)
            XCTAssert(shift.id == "r7vv7ae")
            XCTAssert(shift.hourlyRate == 15)
            XCTAssert(shift.duration == 8)
            XCTAssert(shift.currency == .eur)
            let formatter = DateFormatter.dateTime
            XCTAssert(formatter.string(from: shift.startDate) == "2019-10-04 18:30:00")
            XCTAssert(formatter.string(from: shift.endDate) == "2019-10-05 03:00:00")
        } catch let decodingError as DecodingError {
            XCTFail(decodingError.description)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testJobParsing() {
        let responseDataJSONData = jsonData(fromFileNamed: "Job")
        do {
            let job = try parser.parse(data: responseDataJSONData, as: Job.self, decoder: .smartDateDecoder)
            XCTAssert(job.id == 24781)
            XCTAssert(job.title == "Bediening bruiloft")
            XCTAssert(job.location.latitude == 52.156381)
            XCTAssert(job.location.longitude == 5.363739)
            XCTAssert(job.jobCategory.description == "Serving")
            XCTAssert(job.photoURL == "https://tmpr-photos.ams3.digitaloceanspaces.com/hero/118101.jpg")
            XCTAssert(job.shifts.count == 1)
        } catch let decodingError as DecodingError {
            XCTFail(decodingError.description)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testJobsResponseParsing() {
        let responseJSONData = jsonData(fromFileNamed: "JobsResponse")
        do {
            let response = try parser.parse(data: responseJSONData, as: JobsResponse.self, decoder: .smartDateDecoder)
            let formatter = DateFormatter.short
            XCTAssert(response.data.count == 1)
            XCTAssert(formatter.string(from: response.data.first!.date) == "2019-10-04")
        } catch let decodingError as DecodingError {
            XCTFail(decodingError.description)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    // MARK: HELPERS
    private func jsonData(fromFileNamed fileName: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let jsonURL = bundle.url(forResource: fileName, withExtension: "json") else {
            XCTFail("Failed to locate \(fileName).json")
            fatalError()
        }
        guard let data = try? Data(contentsOf: jsonURL) else {
            XCTFail("\(fileName).json is invalid")
            fatalError()
        }
        return data
    }
}

// MARK: ERROR LOGGING
private extension DecodingError {
    var description: String {
        let description: String
        switch self {
        case .dataCorrupted(let context):
            description = context.debugDescription
        case .keyNotFound(let key, let context):
            description = "\(key.stringValue) was not found, \(context.debugDescription)"
        case .typeMismatch(let type, let context):
            description = "\(type) was expected, \(context.debugDescription), \(context.codingPath)"
        case .valueNotFound(let type, let context):
            description = "no value was found for \(type), \(context.debugDescription), \(context.codingPath)"
        }
        return description
    }
}
